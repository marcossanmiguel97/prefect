# prefect



## Getting started

To use CLI with prefect2:  prefect config set PREFECT_API_URL=http://193.146.75.210:31333/api

prefect deployment build ./models.py:models_flow -n models_flow_demo -t kubernetes -ib kubernetes-job/prod   -q kubernetes -sb github/git-block

prefect deployment build ./flow_cadastral.py:flow_cadastral -n cadastral -t kubernetes -ib kubernetes-job/cadastral   -q kubernetes -sb github/cadastral-filesystem
prefect deployment apply models_flow-deployment.yaml
## Install prefect (local)
```
pip install -U prefect
```
## Install prefect in kubernetes
kubectl apply -k kustomize/

## Run a basic flow
A flow is the basis of all Prefect workflows. A flow is a Python function decorated with a @flow decorator.

A task is a Python function decorated with a @task decorator. Tasks represent distinct pieces of work executed within a flow.

By adding the @flow decorator to a function, function calls will create a flow run — the Prefect Orion orchestration engine manages flow and task state, including inspecting their progress, regardless of where your flow code runs.
```
import requests
from prefect import flow, task

@task
def call_api(url):
    response = requests.get(url)
    print(response.status_code)
    return response.json()

@task
def parse_fact(response):
    fact = response["fact"]
    print(fact)
    return fact

@flow
def api_flow(url):
    fact_json = call_api(url)
    fact_text = parse_fact(fact_json)
    return fact_text

api_flow("https://catfact.ninja/fact") 
```
```
from prefect import task, flow

@task
def printer(obj):
    print(f"Received a {type(obj)} with value {obj}")

# note that we define the flow with type hints
@flow
def validation_flow(x: int, y: str):
    printer(x)
    printer(y)

validation_flow(x="42", y=100)
```

## Prefect orchestration components
https://docs.prefect.io/tutorials/orion/#prefect-orchestration-components

Designing workflows with Prefect starts with a few basic building blocks that you've already seen: flows and tasks.

Creating and running orchestrated workflows takes advantage of some additional Prefect components.

* Prefect Orion API server and orchestration engine receives state information from workflows and provides flow run instructions for executing deployments.
* Prefect database provides a persistent metadata store that holds flow and task run history.
* Prefect UI provides a control plane for monitoring, configuring, analyzing, and coordinating runs of your workflows.
* Storage for flow and task data lets you configure a persistent store for flow code and flow and task results.
* Agents and work queues bridge the Prefect orchestration engine with a your execution environments, organizing work that agents can pick up to execute.

These Prefect components and services enable you to form what we call a dedicated coordination and orchestration environment. The same components and services enable you to coordinate flows with either the open-source Prefect Orion API server and orchestration engine or Prefect Cloud.

## Storage
Storage lets you configure how flow code for deployments is persisted and retrieved by Prefect agents. Anytime you build a deployment, a storage block is used to upload the entire directory containing your workflow code (along with supporting files) to its configured location.

### Supported storage blocks
Current options for deployment storage blocks include:

* Storage	Description	Required Library
* Local File System	Store data in a run's local file system.
* Remote File System	Store data in a any filesystem supported by fsspec.
* AWS S3 Storage	Store data in an AWS S3 bucket.	s3fs
* Azure Storage	Store data in Azure Datalake and Azure Blob Storage.	adlfs
* GitHub Storage	Store data in a GitHub repository.
* Google Cloud Storage	Store data in a Google Cloud Platform (GCP) Cloud Storage bucket.	gcsfs
* SMB	Store data in SMB shared network storage.	smbprotocol

## Agents and work queues
Agents and work queues bridge the Prefect Orion orchestration engine and API with your local execution environments.

* Work queues contain all the logic about what flows run and how. Agents just pick up work from queues and execute the flows.
* There is no global agent that picks up orchestrated work by default. You must configure an agent with a specific work queue.Agents run in a local execution environment. They pick up work from a specific work queue and execute those flow runs.

## Deployments
Deployments take your flows to the next level: adding the information needed for scheduling flow runs or triggering a flow run via an API call. Deployments elevate workflows from functions that you call manually to API-managed entities.

### Components of a deployment
You need just a few ingredients to turn a flow definition into a deployment:

* A Python script that contains a function decorated with @flow
That's it. To create flow runs based on the deployment, you need a few more pieces:

* Prefect orchestration engine, either Prefect Cloud or a local Prefect Orion server started with prefect orion start (for this use case we use prefect deployed in kubernetes).
* An agent and work queue.
These all come with Prefect. You just have to configure them and set them to work. You'll see how to configure each component during this tutorial.

Optionally, you can configure storage for packaging and saving your flow code and dependencies.

### Deployment creation with the Prefect CLI
To create a deployment from an existing flow script using the CLI, there are just a few steps:

1. Use the `prefect deployment build` Prefect CLI command to create a deployment definition YAML file. By default this step also uploads your flow script and any supporting files to storage, if you've specified storage for the deployment.
2. Optionally, before applying, you can edit the deployment YAML file to include additional settings that are not easily specified via CLI flags.
3.  Use the `prefect deployment apply` Prefect CLI command to create the deployment with the Prefect Orion server based on the settings in the deployment YAML file.

#### Deployment example
```
prefect deployment build ./python_file.py:flow_function -n name_for_deployment -q name_of_workqueue
```
After this command, a YAML file is created.
```
prefect deployment apply yaml_file.yaml
```
## Infrastructure
Users may specify an infrastructure block when creating a deployment. This block will be used to specify infrastructure for flow runs created by the deployment at runtime.

Infrastructure can only be used with a deployment. When you run a flow directly by calling the flow yourself, you are responsible for the environment in which the flow executes.

Infrastructure is specific to the environments in which flows will run. Prefect currently provides the following infrastructure types:

* Process runs flows in a local subprocess.
* DockerContainer runs flows in a Docker container.
* KubernetesJob runs flows in a Kubernetes Job.
* ECSTask runs flows in an ECS Task.

You can create,  for example a KubernetesJob block for infrastructure:
```
from prefect.infrastructure import KubernetesJob
from prefect.filesystems import GitHub

k8s = KubernetesJob(
    image="marcuus/prefect_model:latest",
    image_pull_policy="Always",
    env={"EXTRA_PIP_PACKAGES": "matplotlib pandas scipy sklearn xgboost"}
)
k8s.save("prod", overwrite=True)
```
This block will be specified in prefect deployment build with `-ib kubernetes-job/prod`.

KubernetesJob infrastructure executes flow runs in a Kubernetes Job.

### Requirements for KubernetesJob:

* kubectl must be available.
* You must configure remote Storage. Local storage is not supported for Kubernetes.
* The ephemeral Orion API won't work with Docker and Kubernetes. You must have an Orion or Prefect Cloud API endpoint set in your agent's configuration.
## References
https://docs.prefect.io