from prefect.infrastructure import KubernetesJob
from prefect.filesystems import GitHub



k8s = KubernetesJob(
    image="marcuus/prefect_model:latest",
    image_pull_policy="Always",
#    env={"EXTRA_PIP_PACKAGES": "matplotlib, scipy, xgboost, sklearn"},
    env={"EXTRA_PIP_PACKAGES": "matplotlib pandas scipy scikit-learn==1.1.1. xgboost"}
)
k8s.save("prodv2", overwrite=True)
