# -*- coding: utf-8 -*-

# Copyright 2022 Spanish National Research Council (CSIC)
#
# Licensed under the Apache License, Version 2.0 (the "License"); you may
# not use this file except in compliance with the License. You may obtain
# a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
# WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
# License for the specific language governing permissions and limitations
# under the License.

"""
Train and test ML and population models. Aggregate using the simple mean.
Predictions are performed for blocks of n days.
"""


from typing import Union
import numpy as np
import scipy.optimize
import pandas as pd
from sklearn import preprocessing
from sklearn.ensemble import RandomForestRegressor
from sklearn.model_selection import GridSearchCV
from sklearn.neighbors import KNeighborsRegressor
from sklearn.kernel_ridge import KernelRidge
import xgboost as xgb


class Model:
    """
    Class Model: create a machine learning or a population model.
    For the case of the population models, the available options are:
    'Gompertz', 'Bertalanffy', 'Logistic', 'Richards' or 'All' (including all the previous ones).
    In this approach, the models are only fed with the data on daily cases.
    In the case of ML models, other types of data can be included in addition to the daily cases.
    These will be entered in the variable 'other_data', and the type of data (e.g. weather,
    vaccines, etc.) will be included in 'kind_data'. The following ML models will be used:
    Random Forest, Gradient Boosting (using xgboost), kNN and KRR.
    In both cases, the way to aggregate the resulting predictions will be a simple average.
    """
    MAX_DATA_POP = 30  # We recommend taking a maximum of 30 days of data
    POP_COORD = [8, 17, 26]
    N_LAGS = 14
    DATA_VAL = 30
    DATA_TEST = 92

    def __init__(self,
                 data: list,
                 t: list,
                 verbose: int = 0,
                 pop_model: bool = True,
                 ml_model: bool = False,
                 model_name: str = '',
                 other_data: Union[list, bool] = False,
                 kind_data: Union[list, bool] = False,
                 days_pred: int = 14):
        self.verbose = verbose
        self.model_name = model_name
        self.pop_model = pop_model
        self.ml_model = ml_model
        self.data = data
        self.t = t
        self.other_data = other_data
        self.kind_data = kind_data
        self.days_pred = days_pred

    def population_model(self, days_pred: int):
        """
        Trains the population model introduce in self.model_name and predicts for the test set.

        :param days_pred: number of days to predict.
        :return: prediction for the test set (pred_int, which is a list).
        If self.model_name is 'All' returns the prediction obtained with each population
        model (pred_pop), the aggregated one (agg_pred_pop), and the names of the models used (models).
        """
        if self.pop_model:
            data = self.data[-self.MAX_DATA_POP:]
            data = np.cumsum(data)
            t = np.array(list(range(len(data))))

            i, j, k = self.POP_COORD
            h = t[j] - t[i]

            if self.model_name == 'All':
                predictions = []
                models = []
                rmse = []

            if self.model_name in ('Gompertz', 'All'):
                alfa = np.log(data[j] / data[i]) / np.log(data[k] / data[i])
                b = np.log(alfa / (1 - alfa)) / h
                c = np.log(data[j] / data[i]) / (np.exp(-b * t[j]) - np.exp(- b * t[i]))
                a = b * (np.log(data[i]) - c * np.exp(-b * t[i]))

                gompertz_fun = lambda x: sum((np.exp(x[2] * np.exp(-x[1] * t) + x[0] / x[1]) - data) ** 2)
                xopt = scipy.optimize.fmin(func=gompertz_fun, x0=[a, b, c], maxiter=10000)

                pred_train = np.exp(xopt[2] * np.exp(-xopt[1] * t) + (xopt[0] / xopt[1]))
                rmse_gomp = np.sqrt(np.mean((data - pred_train) ** 2))

                t_next = np.array([t[-1] + d for d in range(1, days_pred + 1)])
                pred = np.exp(xopt[2] * np.exp(-xopt[1] * t_next) + (xopt[0] / xopt[1]))

                pred_int = [round(_pred) for _pred in pred]
                _pred_int = []
                for coord in range(len(pred_int)):
                    if coord == 0:
                        _pred_int.append(pred_int[coord] - round(pred_train[-1]))
                    else:
                        _pred_int.append(pred_int[coord] - pred_int[coord - 1])
                pred_int = _pred_int
                pred_int = [0 if pred < 0 else pred for pred in pred_int]
                if pred_int[0] == 0:
                    last_pred_train = round(pred_train[-1]) - round(pred_train[-2])
                    if np.mean([last_pred_train, pred_int[1]]) > 0:
                        pred_int[0] = round(np.mean([last_pred_train, pred_int[1]]))

                if self.model_name == 'All':
                    predictions.append(pred_int)
                    models.append('Gompertz')
                    rmse.append(rmse_gomp)
                else:
                    return pred_int

            if self.model_name in ('Bertalanffy', 'All'):

                alfa = (data[j] ** (1 / 4) - data[i] ** (1 / 4)) / (data[k] ** (1 / 4) - data[i] ** (1 / 4))
                b = (-4 / h) * np.log((1 - alfa) / alfa)
                c = (data[j] ** (1 / 4) - data[i] ** (1 / 4)) / (np.exp(-b * t[j] / 4) - np.exp(-b * t[i] / 4))
                a = (data[i] ** (1 / 4) - c * np.exp(-b * t[i] / 4)) * b

                bertalanffy_fun = lambda x: sum((((x[0] / x[1] + x[2] * np.exp(-x[1] * t / 4)) ** 4) - data) ** 2)
                xopt = scipy.optimize.fmin(func=bertalanffy_fun, x0=[a, b, c], maxiter=10000)

                pred_train = (xopt[0] / xopt[1] + xopt[2] * np.exp(-xopt[1] * t / 4)) ** 4
                rmse_bert = np.sqrt(np.mean((data - pred_train) ** 2))

                t_next = np.array([t[-1] + d for d in range(1, days_pred + 1)])
                pred = (xopt[0] / xopt[1] + xopt[2] * np.exp(-xopt[1] * t_next / 4)) ** 4

                pred_int = [round(_pred) for _pred in pred]
                _pred_int = []
                for coord in range(len(pred_int)):
                    if coord == 0:
                        _pred_int.append(pred_int[coord] - round(pred_train[-1]))
                    else:
                        _pred_int.append(pred_int[coord] - pred_int[coord - 1])
                pred_int = _pred_int
                pred_int = [0 if pred < 0 else pred for pred in pred_int]
                if pred_int[0] == 0:
                    last_pred_train = round(pred_train[-1]) - round(pred_train[-2])
                    if np.mean([last_pred_train, pred_int[1]]) > 0:
                        pred_int[0] = round(np.mean([last_pred_train, pred_int[1]]))

                if self.model_name == 'All':
                    predictions.append(pred_int)
                    models.append('Bertalanffy')
                    rmse.append(rmse_bert)
                else:
                    return pred_int

            if self.model_name in ('Logistic', 'Richards', 'All'):

                alfa = (1 / data[j] - 1 / data[i]) / (1 / data[k] - 1 / data[i])
                a = np.log(alfa / (1 - alfa)) / h
                c = (1 / data[j] - 1 / data[i]) / (np.exp(-a * t[j]) - np.exp(-a * t[i]))
                b = a / data[i] - a * c * np.exp(-a * t[i])

                logistic_fun = lambda x: sum((1 / (x[2] * np.exp(-x[0] * t) + x[1] / x[0]) - data) ** 2)
                xopt = scipy.optimize.fmin(func=logistic_fun, x0=[a, b, c], maxiter=10000)

                t_next = np.array([t[-1] + d for d in range(1, days_pred + 1)])

                if self.model_name == 'Logistic' or self.model_name == 'All':

                    pred_train = 1 / (xopt[2] * np.exp(-xopt[0] * t) + xopt[1] / xopt[0])
                    rmse_log = np.sqrt(np.mean((data - pred_train) ** 2))

                    pred = 1 / (xopt[2] * np.exp(-xopt[0] * t_next) + xopt[1] / xopt[0])
                    pred_int = [round(_pred) for _pred in pred]
                    _pred_int = []
                    for coord in range(len(pred_int)):
                        if coord == 0:
                            _pred_int.append(pred_int[coord] - round(pred_train[-1]))
                        else:
                            _pred_int.append(pred_int[coord] - pred_int[coord - 1])
                    pred_int = _pred_int
                    pred_int = [0 if pred < 0 else pred for pred in pred_int]
                    if pred_int[0] == 0:
                        last_pred_train = round(pred_train[-1]) - round(pred_train[-2])
                        if np.mean([last_pred_train, pred_int[1]]) > 0:
                            pred_int[0] = round(np.mean([last_pred_train, pred_int[1]]))

                    if self.model_name == 'All':
                        predictions.append(pred_int)
                        models.append('Logistic')
                        rmse.append(rmse_log)
                    else:
                        return pred_int

                if self.model_name in ('Richards', 'All'):
                    a = xopt[0]
                    b = xopt[1]
                    c = xopt[2]
                    s = 1

                    richards_fun = lambda x: sum(
                        (1 / (x[2] * np.exp(-x[0] * t) + (x[1] / x[0]) ** x[3]) ** (1 / x[3]) - data) ** 2)
                    xopt = scipy.optimize.fmin(func=richards_fun, x0=[a, b, c, s], maxiter=10000)

                    pred_train = 1 / (xopt[2] * np.exp(-xopt[0] * t) + (xopt[1] / xopt[0]) ** xopt[3]) ** (1 / xopt[3])
                    rmse_rich = np.sqrt(np.mean((data - pred_train) ** 2))

                    pred = 1 / (xopt[2] * np.exp(-xopt[0] * t_next) + (xopt[1] / xopt[0]) ** xopt[3]) ** (1 / xopt[3])
                    pred_int = [round(_pred) for _pred in pred]

                    _pred_int = []
                    for coord in range(len(pred_int)):
                        if coord == 0:
                            _pred_int.append(pred_int[coord] - round(pred_train[-1]))
                        else:
                            _pred_int.append(pred_int[coord] - pred_int[coord - 1])
                    pred_int = _pred_int
                    pred_int = [0 if pred < 0 else pred for pred in pred_int]
                    if pred_int[0] == 0:
                        last_pred_train = round(pred_train[-1]) - round(pred_train[-2])
                        if np.mean([last_pred_train, pred_int[1]]) > 0:
                            pred_int[0] = round(np.mean([last_pred_train, pred_int[1]]))

                    if self.model_name == 'All':
                        predictions.append(pred_int)
                        models.append('Richards')
                        rmse.append(rmse_rich)
                    else:
                        return pred_int

            if self.model_name == 'All':
                print(predictions)
                mean_pred = []
                # rmse = np.array(rmse)
                # pesos = 1 / (rmse / sum(rmse))
                for i in range(len(predictions[0])):
                    pred_i = []
                    for j in range(len(predictions)):
                        pred_i.append(predictions[j][i])
                    mean_pred.append(round(np.mean(pred_i)))

                pred_pop = predictions
                agg_pred_pop = mean_pred
                return pred_pop, agg_pred_pop, models

        else:
            raise ValueError("The model introduced is not an ODE-based model.")

    def pred_train(self,
                   x_train: list,
                   y_train: list,
                   y_val: list,
                   n: int,
                   model,
                   scaler):
        """
        Predicts for the train set for each set of n days using the given ML model.

        :param x_train: X train features.
        :param y_train: y train data (labels).
        :param y_val: y validation data (labels).
        :param n: number of days to predict
        :param model: ML model used.
        :param scaler: scaler for the features
        :return: mean RMSE and prediction for the train set (predicting each n days).
        """
        y = np.concatenate([y_train, y_val])
        n_pred = len(x_train)
        rmse = []
        pred_train = []
        for week in range(n_pred):
            pred_week = []
            data = y[week:week + n]
            next_x = []
            for j in range(n):
                if j == 0:
                    next_x = x_train[week]
                    next_x_scal = scaler.transform(next_x.reshape(1, -1))
                    pred = round(model.predict(next_x_scal.reshape(1, -1))[0])
                    pred = max(pred, 0)
                else:
                    next_x_new = [pred]
                    for i in range(self.N_LAGS - 1):
                        next_x_new.append(next_x[i])

                    if self.other_data:
                        for i in range(len(self.kind_data)):
                            next_x_new.append(self.other_data[i][week + j])
                    next_x = np.array(next_x_new)
                    next_x_scal = scaler.transform(next_x.reshape(1, -1))
                    pred = round(model.predict(next_x_scal.reshape(1, -1))[0])
                    pred = max(pred, 0)

                pred_week.append(pred)
            rmse.append(np.sqrt(np.mean((data - pred_week) ** 2)))
            pred_train.append(pred_week)

        return np.mean(rmse), pred_train

    def pred_validation(self,
                        x_train: list,
                        x_val: list,
                        y_val: list,
                        y_test: list,
                        n: int,
                        model,
                        scaler):
        """
        Predicts for the validation set for each set of n days using the given ML model.

        :param x_train: X train features.
        :param x_val: X validation features.
        :param y_val: y validation data (labels).
        :param y_test: y test data (labels).
        :param n: number of days to predict
        :param model: ML model used.
        :param scaler: scaler for the features
        :return: mean RMSE and prediction for the validation set (predicting each n days).
        """
        y = np.concatenate([y_val, y_test])
        n_pred = len(x_val)
        rmse = []
        pred_val = []
        for week in range(n_pred):
            pred_week = []
            data = y[week:week + n]
            next_x = []
            if week == 0:
                for j in range(n):
                    if j == 0:
                        next_x = x_train[-1]
                        next_x_scal = scaler.transform(next_x.reshape(1, -1))
                        pred = round(model.predict(next_x_scal.reshape(1, -1))[0])
                        pred = max(pred, 0)
                    else:
                        next_x_new = [pred]
                        for i in range(self.N_LAGS - 1):
                            next_x_new.append(next_x[i])

                        if self.other_data:
                            for i in range(len(self.kind_data)):
                                next_x_new.append(
                                    self.other_data[i][-(self.DATA_VAL + self.DATA_TEST) - self.N_LAGS + week + j + 1])
                        next_x = np.array(next_x_new)
                        next_x_scal = scaler.transform(next_x.reshape(1, -1))
                        pred = round(model.predict(next_x_scal.reshape(1, -1))[0])
                        pred = max(pred, 0)

                    pred_week.append(pred)
                rmse.append(np.sqrt(np.mean((data - pred_week) ** 2)))
                pred_val.append(pred_week)
            else:
                for j in range(n):
                    if j == 0:
                        next_x = x_val[week]
                        next_x_scal = scaler.transform(next_x.reshape(1, -1))
                        pred = round(model.predict(next_x_scal.reshape(1, -1))[0])
                        pred = max(pred, 0)
                    else:
                        next_x_new = [pred]
                        for i in range(self.N_LAGS - 1):
                            next_x_new.append(next_x[i])

                        if self.other_data:
                            for i in range(len(self.kind_data)):
                                next_x_new.append(
                                    self.other_data[i][-(self.DATA_VAL + self.DATA_TEST) - self.N_LAGS + week + j + 1])
                        next_x = np.array(next_x_new)
                        next_x_scal = scaler.transform(next_x.reshape(1, -1))
                        pred = round(model.predict(next_x_scal.reshape(1, -1))[0])
                        pred = max(pred, 0)

                    pred_week.append(pred)

                rmse.append(np.sqrt(np.mean((data - pred_week) ** 2)))
                pred_val.append(pred_week)

        return np.mean(rmse), pred_val

    def pred_test(self, x_val, x_test, y_test, n, model, scaler):
        """
        Predicts for the test set for each set of n days using the given ML model.

        :param x_val: X validation features.
        :param x_test: X test features.
        :param y_test: y test data (labels).
        :param n: number of days to predict
        :param model: ML model used.
        :param scaler: scaler for the features
        :return: mean RMSE and prediction for the test set (predicting each n days).
        """
        n_pred = len(x_test) - n + 1
        rmse = []
        predictions_test = []
        for week in range(n_pred):
            pred_week = []
            data = y_test[week:week + n]
            next_x = []
            if week == 0:
                for j in range(n):
                    if j == 0:
                        next_x = x_val[-1]
                        next_x_scal = scaler.transform(next_x.reshape(1, -1))
                        pred = round(model.predict(next_x_scal.reshape(1, -1))[0])
                        pred = max(pred, 0)
                    else:
                        next_x_new = [pred]
                        for i in range(self.N_LAGS - 1):
                            next_x_new.append(next_x[i])

                        if self.other_data:
                            for i in range(len(self.kind_data)):
                                next_x_new.append(self.other_data[i][-self.DATA_TEST - self.N_LAGS + week + j + 1])
                        next_x = np.array(next_x_new)
                        next_x_scal = scaler.transform(next_x.reshape(1, -1))
                        pred = round(model.predict(next_x_scal.reshape(1, -1))[0])
                        pred = max(pred, 0)

                    pred_week.append(pred)
                rmse.append(np.sqrt(np.mean((data - pred_week) ** 2)))
                predictions_test.append(pred_week)
            else:
                for j in range(n):
                    if j == 0:
                        next_x = x_test[week]
                        next_x_scal = scaler.transform(next_x.reshape(1, -1))
                        pred = round(model.predict(next_x_scal.reshape(1, -1))[0])
                        pred = max(pred, 0)
                    else:
                        next_x_new = [pred]
                        for i in range(self.N_LAGS - 1):
                            next_x_new.append(next_x[i])

                        if self.other_data:
                            for i in range(len(self.kind_data)):
                                next_x_new.append(self.other_data[i][-self.DATA_TEST - self.N_LAGS + week + j + 1])
                        next_x = np.array(next_x_new)
                        next_x_scal = scaler.transform(next_x.reshape(1, -1))
                        pred = round(model.predict(next_x_scal.reshape(1, -1))[0])
                        pred = max(pred, 0)

                    pred_week.append(pred)

                rmse.append(np.sqrt(np.mean((data - pred_week) ** 2)))
                predictions_test.append(pred_week)

        return predictions_test, np.mean(rmse)

    def pred_ml_model(self, return_models: bool = False):
        """
        Predict using the four ML models under study: Random Forest, Gradient Boosting, kNN and KRR.

        :param return_models: if true, returns the trained models together with the train and test data,
        in order to interpret the results obtained.
        :return: if return models is false:
            y_train: train labels.
            p_train: train predictions with all the ML models.
            models_ml: name of the models used.
            y_val: validation labels.
            pred_val: validation predictions with all the ML models.
            y_test: test labels.
            pred_test_ml: test predictions with all the ML models.
            agg_pred_test_ml: aggrgated prediction for the test set.
            rmse_test: RSME for the test set with the aggregated prediction.
            mape_test: MAPE for the test set with the aggregated prediction.
            mape_day: MAE obtained each day for the test set with the aggregated prediction.
        If return models is true, in addition to the previous outputs:
            data_test: data used for testing.
            data_train: data used for training.
            models_trained: ML models trained.
            parameters: the name of the features used.
        """
        if self.ml_model:
            df = pd.DataFrame({'Date': self.t, 'Data': self.data})
            parameters = []
            for i in range(1, self.N_LAGS + 1):
                lag = 'lag' + str(i)
                df[lag] = df['Data'].shift(i)
                parameters.append(lag)

            df = df.dropna()

            if self.other_data:
                for i in range(len(self.kind_data)):
                    df[self.kind_data[i]] = self.other_data[i][:-self.N_LAGS]
                    parameters.append(self.kind_data[i])

            x = df[parameters].values
            y = df.Data.values

            n = self.days_pred
            x_train = x[:-self.DATA_TEST]
            y_train = y[:-self.DATA_TEST]
            x_test = x[-self.DATA_TEST:]
            y_test = y[-self.DATA_TEST:]

            p_train = []
            # models_ml = ['RF', 'GB', 'kNN', 'KRR']

            x_val = x_train[-self.DATA_VAL:]
            y_val = y_train[-self.DATA_VAL:]

            x_train = x_train[:-self.DATA_VAL]
            y_train = y_train[:-self.DATA_VAL]

            scaler = preprocessing.StandardScaler().fit(x_train)
            x_train_scal = scaler.transform(x_train)

            cv_rf = GridSearchCV(estimator=RandomForestRegressor(random_state=0), cv=5,
                                 param_grid={'n_estimators': np.arange(50, 151, 10),
                                             'max_depth': np.arange(5, 51, 5),
                                             # 'max_features': ['auto', 'sqrt', 'log2'],
                                             })
            cv_rf.fit(x_train_scal, y_train)
            rmse_rf_train, pred_train = self.pred_train(x_train, y_train, y_val,
                                                        n, cv_rf, scaler)
            p_train.append(pred_train)
            rf = cv_rf.best_estimator_
            rmse_val = []
            rmse_val_rf, pred_val_rf = self.pred_validation(x_train, x_val, y_val, y_test,
                                                            n, rf, scaler)
            rmse_val.append(rmse_val_rf)

            r2_rf_train = cv_rf.score(x_train_scal, y_train)

            if self.verbose == 1:
                print(' Random forest --- DONE')
                print(f'R² train: {r2_rf_train}')
                print(f'RMSE train: {rmse_rf_train}')

            cv_gb = GridSearchCV(estimator=xgb.XGBRegressor(random_state=0), cv=5,
                                 param_grid={
                                     'n_estimators': np.arange(10, 51, 10),
                                     # 'loss': ['linear', 'square', 'exponential'],
                                     'learning_rate': [1e-2, 1e-1, 1]
                                 })
            cv_gb.fit(x_train_scal, y_train)
            rmse_gb_train, pred_train = self.pred_train(x_train, y_train, y_val,
                                                        n, cv_gb, scaler)
            p_train.append(pred_train)
            gb = cv_gb.best_estimator_
            rmse_val_gb, pred_val_gb = self.pred_validation(x_train, x_val, y_val, y_test,
                                                            n, gb, scaler)
            rmse_val.append(rmse_val_gb)

            r2_gb_train = cv_gb.score(x_train_scal, y_train)

            if self.verbose == 1:
                print('\n Gradient boosting --- DONE')
                print(f'R² train: {r2_gb_train}')
                print(f'RMSE train: {rmse_gb_train}')

            cv_knn = GridSearchCV(estimator=KNeighborsRegressor(),
                                  cv=5, param_grid={'n_neighbors': np.arange(5, 26)})
            cv_knn.fit(x_train_scal, y_train)
            rmse_knn_train, pred_train = self.pred_train(x_train, y_train, y_val,
                                                         n, cv_knn, scaler)
            p_train.append(pred_train)
            knn = cv_knn.best_estimator_
            rmse_val_knn, pred_val_knn = self.pred_validation(x_train, x_val, y_val, y_test,
                                                              n, knn, scaler)
            rmse_val.append(rmse_val_knn)

            r2_knn_train = cv_knn.score(x_train_scal, y_train)

            if self.verbose == 1:
                print('\n kNN --- DONE')
                print(f'R² train: {r2_knn_train}')
                print(f'RMSE train: {rmse_knn_train}')

            cv_krr = GridSearchCV(KernelRidge(kernel='rbf', gamma=0.5), cv=5,
                                  param_grid={"alpha": [1e-2, 1e-1, 1e0],
                                              "gamma": np.logspace(-7, 7, 5)})
            cv_krr.fit(x_train_scal, y_train)
            rmse_krr_train, pred_train = self.pred_train(x_train, y_train, y_val, n,
                                                         cv_krr, scaler)
            p_train.append(pred_train)
            krr = cv_krr.best_estimator_
            rmse_val_krr, pred_val_krr = self.pred_validation(x_train, x_val, y_val, y_test, n,
                                                              krr, scaler)
            rmse_val.append(rmse_val_krr)

            r2_krr_train = cv_krr.score(x_train_scal, y_train)

            if self.verbose == 1:
                print('\n KRR --- DONE')
                print(f'R² train: {r2_krr_train}')
                print(f'RMSE train: {rmse_krr_train}')

            print(rmse_val)

            predictions_test_rf, rmse_rf = self.pred_test(x_val, x_test, y_test, n, rf, scaler)
            predictions_test_gb, rmse_gb = self.pred_test(x_val, x_test, y_test, n, gb, scaler)
            predictions_test_knn, rmse_knn = self.pred_test(x_val, x_test, y_test, n, knn, scaler)
            predictions_test_krr, rmse_krr = self.pred_test(x_val, x_test, y_test, n, krr, scaler)

            models_ml = ['rf', 'gb', 'knn', 'krr']
            pred_test_ml = [predictions_test_rf, predictions_test_gb, predictions_test_knn,
                            predictions_test_krr]
            pred_val = [pred_val_rf, pred_val_gb, pred_val_knn, pred_val_krr]

            # pesos = 1 / (rmse_val / sum(rmse_val))
            agg_pred_test_ml = []
            rmse_test = []
            mape_test = []
            mape_day = []
            for week in range(len(predictions_test_rf)):
                agg_pred_test_week = []
                for j in range(n):
                    pred_j = []
                    for i in range(len(models_ml)):
                        pred_j.append(pred_test_ml[i][week][j])
                    agg_pred_test_week.append(round(np.mean(pred_j)))
                data = y_test[week:week + n]
                rmse_test.append(np.sqrt(np.mean((data - agg_pred_test_week) ** 2)))
                mae = np.abs(data - agg_pred_test_week)
                mape_mean = np.mean(mae / data)
                mape_day.append(mae / data)
                mape_test.append(mape_mean)
                agg_pred_test_ml.append(agg_pred_test_week)

            if return_models is False:
                return (y_train, p_train, models_ml, y_val, pred_val, y_test, pred_test_ml,
                        agg_pred_test_ml, rmse_test, mape_test, mape_day)
            data_test = x_test
            data_train = x_train_scal
            models_trained = [rf, gb, knn, krr]
            return (y_train, p_train, models_ml, y_val, pred_val, y_test, pred_test_ml,
                    agg_pred_test_ml, rmse_test, mape_test, mape_day,
                    data_test, data_train, models_trained, parameters)

        raise ValueError("The model introduced is not a Machine Learning model.")


def aggregate_models(data: list,
                     t: list,
                     other_data: Union[list, bool] = False,
                     kind_data: Union[list, bool] = False,
                     verbose: int = 0,
                     days_pred: int = 14,
                     return_models: bool = False):
    """
    Aggregate the predictions obtained with both ML and population models.

    :param data: daily data.
    :param t: time (dates).
    :param other_data: other data used for training the ML models.
    :param kind_data: kind of data used for training the ML models (names).
    :param verbose: level of impression. If 1, information about the training process is shown.
    :param days_pred: days to predict.
    :param return_models: boolean which indicates weather to return or not he trained ML models.
    :return: the following information ir returned:
        y_train: train labels (daily cases).
        y_test: test labels (daily cases).
        y_val: validation labels (daily cases).
        pred_train: prediction obtained with each population model (train).
        pred_val: prediction obtained with each population model (validation).
        agg_pred_test_ml: aggregated prediction obtained for the test set with the ML models.
        mape_test_ml: MAPE obtained for the test set with the aggregated prediction of the ML
            models.
        agg_pred_test_pop: aggregated prediction obtained for the test set with the population
            models.
        mape_test_pop: MAPE obtained for the test set with the aggregated prediction of the
            population models.
        mape_test_agg: MAPE obtained for the test set with the aggregated prediction of both
            ML and population models.
        agg_pred: aggregated prediction using both ML and population models.
        mape_day_ml: MAPE obtained each day of the test set with the aggregated prediction of the
            ML models.
        mape_day_pop: MAPE obtained each day of the test set with the aggregated prediction of the
            population models.
        mape_day_agg: MAPE obtained each day of the test set with the aggregated prediction of both
            ML and population models.
        test_weeks: data (daily cases) of the weeks used for testing.
    """
    ml = Model(data, t, verbose=verbose, pop_model=False, ml_model=True,
               other_data=other_data, kind_data=kind_data, days_pred=days_pred)
    (y_train,
     pred_train,
     _,
     y_val,
     pred_val,
     y_test,
     pred_test_ml,
     agg_pred_test_ml,
     _,
     mape_test_ml,
     mape_day_ml) = ml.pred_ml_model(return_models)

    pred_test_pop = []
    agg_pred_test_pop = []
    for week in range(len(mape_test_ml)):
        data_train = data[:-ml.DATA_TEST + week]
        t = t[:-ml.DATA_TEST + week]
        pop = Model(data_train,
                    t,
                    verbose=verbose,
                    pop_model=True,
                    ml_model=False,
                    model_name='All')
        pred_pop, agg_pred_pop = pop.population_model(days_pred=days_pred)
        pred_test_pop.append(pred_pop)
        agg_pred_test_pop.append(agg_pred_pop)

    agg_pred = []
    mape_test_agg = []
    mape_test_pop = []
    mape_day_pop = []
    mape_day_agg = []
    test_weeks = []
    for week in range(len(mape_test_ml)):
        agg_pred_week = []
        data_test = data[-ml.DATA_TEST + week:-ml.DATA_TEST + week + days_pred]
        if -ml.DATA_TEST + week + days_pred == 0:
            data_test = data[-ml.DATA_TEST + week:]

        # mape pop models:
        mae_pop = np.abs(data_test - np.array(agg_pred_test_pop[week]))
        mape_day_pop.append(mae_pop / data_test)
        mape_test_pop.append(np.mean(mae_pop / data_test))

        for i in range(len(agg_pred_test_ml[week])):
            predictions = []
            for model in range(len(pred_test_ml)):
                predictions.append(pred_test_ml[model][week][i])
            for model in range(len(pred_pop)):
                predictions.append(pred_test_pop[week][model][i])
            agg_pred_week.append(round(np.mean(predictions)))
        agg_pred_week = np.array(agg_pred_week)
        mae = np.abs(data_test - agg_pred_week)
        mape_day_agg.append(mae / data_test)
        mape_mean = np.mean(mae / data_test)
        mape_test_agg.append(mape_mean)
        agg_pred.append(agg_pred_week)
        test_weeks.append(data_test)

    return (y_train, y_test, y_val, pred_train, pred_val, agg_pred_test_ml, mape_test_ml,
            agg_pred_test_pop, mape_test_pop, mape_test_agg, agg_pred, mape_day_ml, mape_day_pop,
            mape_day_agg, test_weeks)
