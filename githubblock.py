from prefect.filesystems import GitHub

block = GitHub(
    repository="https://gitlab.com/marcossanmiguel97/prefect/")
block.get_directory("files") # specify a subfolder of repo
block.save("git-block", overwrite=True)
