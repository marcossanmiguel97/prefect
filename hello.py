#prefect 2.0
import prefect
from prefect import task, flow, get_run_logger
from prefect.filesystems import GitHub
import pandas
#github_block = GitHub.load("git-block")
import sklearn
@task
def hello_world():
    logger = get_run_logger()
    text = "hello from orion_flow!"
    logger.info(text)
    return text


@flow(name="orion_flow")
def orion_flow():
    logger = get_run_logger()
    logger.info("Hello from Kubernetes!")
    hw = hello_world()
    return
