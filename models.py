from matplotlib.pyplot import get
import os
import pandas as pd
import numpy as np
import prefect
from prefect import task, flow, Task, get_run_logger
from prefect.infrastructure import KubernetesJob
from prefect.filesystems import GitHub

github_block = GitHub.load("git-block")



from typing import Union
import numpy as np
import pandas as pd
from models_def import *



@task()
def read_data(file_cases):
    logger = get_run_logger()
    logger.info(os.getcwd())
    logger.info(os.system("ls"))
    file_cases = file_cases
    df = pd.read_csv(file_cases)
    df = df[df.fecha >= '2021-01-01']
    df = df[df.fecha < '2022-01-01']
    num_casos_ccaa = df.num_casos
    num_casos = []
    i = 0
    while i < len(num_casos_ccaa):
        num_casos.append(sum(num_casos_ccaa[i:i+19]))
        i += 19
    date = np.unique(df.fecha)
    return date, num_casos

@task
def read_vaccines(file):
    df_vac_2021 = pd.read_csv(file)
    return df_vac_2021

@task
def read_mobility(file):
    df_mobility = pd.read_csv(file)
    return df_mobility

@task
def read_climate(file):
    df_climate = pd.read_csv(file)
    df_climate_2021 = df_climate[df_climate.Date >= '2021-01-01']
    return df_climate_2021

@task()
def get_values(df_vac_2021, df_mobility, df_climate_2021, date):
    firstDose = df_vac_2021.firstDoseDaily.values
    secondDose = df_vac_2021.secondDoseDaily.values
    flux_mobility = df_mobility.flux.values
    temp_rolling_avg = df_climate_2021.temp_rolling_avg.values
    precip_rolling_avg = df_climate_2021.precip_rolling_avg.values
    firstDose = firstDose[:len(date)]
    secondDose = secondDose[:len(date)]
    flux_mobility = flux_mobility[:len(date)]
    temp_rolling_avg = temp_rolling_avg[:len(date)]
    precip_rolling_avg = precip_rolling_avg[:len(date)]
    other_data = [firstDose, secondDose, flux_mobility, temp_rolling_avg, precip_rolling_avg]
    kind_data = ['1stDose', '2ndDose', 'flux_mobility', 'temp_rolling_avg', 'precip_rolling_avg']
    return other_data, kind_data
@task
def model_task(num_casos, date, other_data, kind_data, days_pred, verbose = 1, pop_model = False, ml_model = True):
        ml = Model(num_casos, date, verbose = 1, pop_model = False, ml_model = True,
                other_data=other_data , kind_data=kind_data , days_pred=days_pred)
        return ml

#model_task = Flow_model(name="Models")

@task
def get_model(ml, date, days_pred):
    _, pred_train_ml, models_ml, _, pred_val_ml, _, pred_test_ml, _, _, _, _= ml.pred_ml_model()
    pred_train_rf = pred_train_ml[0]
    pred_train_gb = pred_train_ml[1]
    pred_train_knn = pred_train_ml[2]
    pred_train_krr = pred_train_ml[3]

    pred_test_rf = pred_test_ml[0]
    pred_test_gb = pred_test_ml[1]
    pred_test_knn = pred_test_ml[2]
    pred_test_krr = pred_test_ml[3]

    pred_val_rf = pred_val_ml[0]
    pred_val_gb = pred_val_ml[1]
    pred_val_knn = pred_val_ml[2]
    pred_val_krr = pred_val_ml[3]

    date_train = date[days_pred-1:-(ml.DATA_TEST )-(ml.DATA_VAL) - 1]
    date_val = date[-(ml.DATA_TEST) -(ml.DATA_VAL) -1: -(ml.DATA_TEST) -1]
    date_test = date[-(ml.DATA_TEST) - 1:-days_pred]


    df_rf_train = pd.DataFrame(pred_train_rf, columns = [f'Day t+{i + 1}' for i in range(days_pred)])
    df_rf_val = pd.DataFrame(pred_val_rf, columns = [f'Day t+{i + 1}' for i in range(days_pred)])
    df_rf_test = pd.DataFrame(pred_test_rf, columns = [f'Day t+{i + 1}' for i in range(days_pred)])
    df_rf = pd.concat([df_rf_train, df_rf_val, df_rf_test])
    #df_rf = df_rf.reset_index()
    df_rf['Date t'] = np.concatenate([date_train, date_val, date_test])
    df_rf

    df_gb_train = pd.DataFrame(pred_train_gb, columns = [f'Day t+{i + 1}' for i in range(days_pred)])
    df_gb_val = pd.DataFrame(pred_val_gb, columns = [f'Day t+{i + 1}' for i in range(days_pred)])
    df_gb_test = pd.DataFrame(pred_test_gb, columns = [f'Day t+{i + 1}' for i in range(days_pred)])
    df_gb = pd.concat([df_gb_train, df_gb_val, df_gb_test])
    #df_gb = df_gb.reset_index()
    df_gb['Date t'] = np.concatenate([date_train, date_val, date_test])
    df_gb

    df_knn_train = pd.DataFrame(pred_train_knn, columns = [f'Day t+{i + 1}' for i in range(days_pred)])
    df_knn_val = pd.DataFrame(pred_val_knn, columns = [f'Day t+{i + 1}' for i in range(days_pred)])
    df_knn_test = pd.DataFrame(pred_test_knn, columns = [f'Day t+{i + 1}' for i in range(days_pred)])
    df_knn = pd.concat([df_knn_train, df_knn_val, df_knn_test])
    #df_knn = df_knn.reset_index()
    df_knn['Date t'] = np.concatenate([date_train, date_val, date_test])
    df_knn

    df_krr_train = pd.DataFrame(pred_train_krr, columns = [f'Day t+{i + 1}' for i in range(days_pred)])
    df_krr_val = pd.DataFrame(pred_val_krr, columns = [f'Day t+{i + 1}' for i in range(days_pred)])
    df_krr_test = pd.DataFrame(pred_test_krr, columns = [f'Day t+{i + 1}' for i in range(days_pred)])
    df_krr = pd.concat([df_krr_train, df_krr_val, df_krr_test])
    #df_krr = df_krr.reset_index()
    df_krr['Date t'] = np.concatenate([date_train, date_val, date_test])
    print(df_krr)
    return None

@flow(name="models_flow")
def models_flow(file_cases ='/opt/prefect/files/casos_tecnica_ccaa.csv', file_vaccines= '/opt/prefect/files/data_vaccines_2021_daily_spline3.csv', file_mobility = '/opt/prefect/files/mobility_data_ESP_INE.csv', file_climate = '/opt/prefect/files/data_climate_esp_2021.csv', days_pred = 14):

    date, num_casos = read_data(file_cases)
    df_vac_2021 = read_vaccines(file_vaccines)
    df_mobility = read_mobility(file_mobility)
    df_climate_2021 = read_climate(file_climate)
    other_data, kind_data = get_values(df_vac_2021, df_mobility, df_climate_2021, date)
    models = model_task(num_casos = num_casos, date = date, other_data = other_data, kind_data = kind_data, days_pred=days_pred)
    results = get_model(models, date, days_pred)

    #Docker(ignore_healthchecks=True)

    #flow.visualize()
    #flow.register('Test_cadastral', add_default_labels=False)
    #flow.run()
    #executor = DaskExecutor('tcp://172.16.64.1:34063')
    #flow.run(executor=executor)
    #flow.visualize()
    #flow.executor=executor
